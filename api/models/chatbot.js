/**
 *
 * generated Model for chatbot
 *
 **/

 module.exports = {
    autoCreatedAt: false,
    autoUpdatedAt: false, 
    attributes: {
        senderid : {
            columnName: 'senderid',
            type: 'string',
        },
        recieverid : {
            columnName: 'recieverid',
            type: 'string',
        },
        message : {
            columnName: 'message',
            type: 'string',
        },
        time: {
            columnName: 'time',
            type: 'string',
        },
    }
}