/**
 * ChatbotController
 *
 * @description :: Server-side logic for managing chatbots
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */


module.exports = {

	startChat : function(req,res)
	{
		var messageData = {
			recipient: { id: sails.config.connections.senderId },
			message: { text: "May i know your age?"	}
		};

		chatService.sendMessage(messageData, function(result)
		{
			return res.json(result);
		});		
	},

	replyChat : function(req, res)
	{
		

		var data = req.body;

  // Make sure this is a page subscription
  if (data.object == 'page') {
    // Iterate over each entry
    // There may be multiple if batched
    data.entry.forEach(function(pageEntry) {
    	var pageID = pageEntry.id;
    	var timeOfEvent = pageEntry.time;

      // Iterate over each messaging event
      pageEntry.messaging.forEach(function(messagingEvent) {
      	if (messagingEvent.message) {
      		chatService.receivedMessage(messagingEvent,function(result){

      		});
      	} else {
      		console.log("Webhook received unknown messagingEvent: ", messagingEvent);
      	}
      });
  });

    // Assume all went well.
    //
    // You must send back a 200, within 20 seconds, to let us know you've 
    // successfully received the callback. Otherwise, the request will time out.
    res.json({status:200,message:'done'});
  }
  else
  {
  	res.json({status:200,message:'done'});
  }

	},

  dbquery : function(req , res)
  {

    chatbot.find().exec(function(err,result)
      {
         console.log(err,result);
         res.json({status:200 , data: result});
      });

    // chatbot.create({senderid:'fdsfsd343',recieverid: 'fgdfgfdg454546546',message:'welcome'}).exec(function(err,result)
    //   {
    //      console.log(err,result);
    //      res.json({status:200 , message: 'successfully done'});
    //   });

  },
	
};

