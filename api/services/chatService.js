var PAGE_ACCESS_TOKEN = "EAAZAJssPjD8oBAARw7xZB41bntUzdbaujB0b1ysUlspYcsOrIlUSzt6gMZAasAuhRHeG0rpq79F7sxIyjPsKXNZADLH2Q4MXtCWZCpcVOSaj4oz3GDmQoGsD0cqyE6RVmYmnKWK5wyT6W3MRzTcUHnw66Ku9WzFfw3Q6P9lqY18atkZCrcJX9i";

var request = require('request');

module.exports = {

	sendMessage : function(messageData,calback)
	{
		request({
			uri: 'https://graph.facebook.com/v2.9/me/messages',
			qs: { access_token: PAGE_ACCESS_TOKEN },
			method: 'POST',
			json: messageData

		}, function (error, response, body) 
		{
			if (!error && response.statusCode == 200)
			{
				var recipientId = body.recipient_id;
				var messageId = body.message_id;
				chatbot.create({senderid:'self454545454',recieverid: recipientId, message:messageData.message.text ,time : new Date()}).exec(function(err,result)
				{       
				});
				return calback({status:200,message:"message sent successfully"});				
			} 
			else 
			{
				return calback({status:300,message:"unable to send message"});
			}
		});
	},

	receivedMessage : function(event, calback)
	{
		var senderID = event.sender.id;
		var recipientID = event.recipient.id;
		var timeOfMessage = event.timestamp;
		var message = event.message;

		var isEcho = message.is_echo;
		var messageId = message.mid;
		var appId = message.app_id;
		var metadata = message.metadata;

		  // You may get a text or attachment but not both
		  var messageText = message.text;
		  var messageAttachments = message.attachments;
		  var quickReply = message.quick_reply;	

		  if (isEcho) 
		  {
		  	return;
		  }
		  else if (quickReply) 
		  {
		  	return;
		  }

		chatbot.create({senderid:senderID,recieverid: recipientID,message:messageText,time:timeOfMessage}).exec(function(err,result)
	      {       
	      });

		  var tempTextMsg = "This is not a valid age";
		  
		  if(!isNaN(messageText) && messageText<18)
		  {
		  		tempTextMsg = "You are not eligible for voting";
		  }
		  else if(!isNaN(messageText) && messageText>=18 && messageText<150)
		  {
		  	tempTextMsg = "You are eligible for voting";
		  }
		
		var messageData = {
			recipient : { id: senderID },
			message : { text: tempTextMsg }
		};

		chatService.sendMessage(messageData, function(result)
		{
			return calback(result);
		});

	},
	
}